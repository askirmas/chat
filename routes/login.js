const {User} = require('../models/user'),
  {HttpError} = require('../error'),
  {AuthError} = require('../models/user')

exports.get = (req, res) =>
  res.render('login')

exports.post = (req, res, next) => {
  const {username, password} = req.body
  User.autorize(username, password, (err, user) => {
    if (err) {
      if (err instanceof AuthError)
        return next(new HttpError(403, err.message))
      else
        return next(err)
    }
    req.session.user = user._id
    res.send({})  
  })
}