const crypto = require('crypto'),
  util = require('util'),
  async = require('async'),
  mongoose = require('../lib/mongoose'),
  {Schema} = mongoose

const schema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  hashedPassword: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    require: true
  },
  created: {
    type: Date,
    default: Date.now
  }
})

schema.methods.encryptPassword = function(password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex')
}

// 'virtual' means doesn't goes to DB - instead setter and getter are executed
schema.virtual('password')
.set(function(password) {
  this._plainPassword = password
  this.salt = Math.random() + ''
  this.hashedPassword = this.encryptPassword(password)
})
.get(function() { return this._plainPassword })

schema.methods.checkPassword = function(password) {
  return this.encryptPassword(password) == this.hashedPassword
}
schema.statics.autorize = function(username, password, cb) {
  var User = this
  async.waterfall(
    [
      cb => User.findOne({username}, cb),
      (user, cb) => {
        if (user)
          if (user.checkPassword(password))
            cb(null, user)
          else
            cb(new AuthError('Wrong password'))
        else {
          const newUser = new User({username, password})
          newUser.save(err => {
            if (err)
              return cb(err)
            cb(null, newUser)
          })
        } 
      }
    ],
    cb
  )
}
exports.User = mongoose.model('User', schema)

function AuthError(message = "Error") {
  Error.apply(this, arguments)
  Error.captureStackTrace(this, AuthError)
  this.message = message
}
util.inherits(AuthError, Error)
AuthError.prototype.name = 'AuthError'
exports.AuthError = AuthError