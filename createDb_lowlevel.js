const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://127.0.0.1:27017';

// Database Name
const dbName = 'chat';

// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
  console.error(err)
  assert.equal(null, err);
  console.log("Connected successfully to server");

  const db = client.db(dbName);
  const collection = db.collection('test_insert')
  collection.insertOne({a:2}, (err, docs) =>
    collection.count((err, count) => console.log(`count = ${count}`))
  )
  collection.find().toArray((err, result) => {
    console.log(result)
    client.close();
  })
  
});