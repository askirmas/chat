const express = require('express'),
  http = require('http'),
  path = require('path'),
  mongoose = require('./lib/mongoose'),
// to use 'config' add "env": {"NODE_PATH": "."} to launch
  config = require('./config'), 
  log = require('./lib/log')(module),
  {HttpError} = require('./error')

const app = express()
app.engine('ejs', require('ejs-locals'))
app.set('views', path.join(__dirname, 'template'))
app.set('view engine', 'ejs')
app.use(express.favicon())
// it isn't immidiate, it goes to res.end() so could be lost if wasn't request wasn't ended 
app.use(express.logger(app.get('env') == 'development' ? 'dev' : 'default'))
//goes to req.body
app.use(express.bodyParser())
/*app.use(express.json());
app.use(express.urlencoded());*/

//goes to req.cookies
const secret = config.get('session:secret'),
  MongoStore = require('connect-mongo')(express)
app.use(express.cookieParser(/*secret - change nothing*/))
app.use(express.session({
  secret,
  key: config.get('session:key'),
  cookie: config.get('session:cookie'),
  store: new MongoStore({mongooseConnection: mongoose.connection})
}))

app.use(require('./middleware/sendHttpError'))
app.use(require('./middleware/loadUser'));

//methods handler
app.use(app.router)
//i.e. - or app.post
require('./routes')(app)

//in general return static is not nodejs case but why not 
app.use(express.static(path.join(__dirname, 'public')));

const server = http.createServer(app)

const io = require('socket.io').listen(server)
io.set('transports', ['websocket'])
io.on('connection', socket => {
  socket.on('message', (data, cb) => {
    socket.broadcast.emit('message', data)
    cb(1)
  })
})

server.listen(config.get('port'), () =>
  log.info('Express server listening on port ' + config.get('port'))
)

app.use((err, req, res, next) => {
  if (typeof err === 'number')
    err = new HttpError(err)
  if (err instanceof HttpError)
    res.sendHttpError(err)
  else if (app.get('env') === 'development')
    express.errorHandler()(err, req, res, next)
  else {
    log.error(err)
    err = new HttpError(500)
    res.sendHttpError(err)
  }
})

