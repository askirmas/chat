const async = require('async'),
  mongoose = require('./lib/mongoose')

async.series([
  open,
  dropDatabase,
  requireModels,
  createUsers
], (err, result) => {
  mongoose.disconnect()
  if (err)
    console.error(err)
  else
    console.log(result)
  process.exit(err ? 255 : 0)
})

function open(callback) {
  mongoose.connection.on('open', callback)
}

function dropDatabase(callback) {
  mongoose.connection.db.dropDatabase(callback)
}

function requireModels(callback) {
  require('./models/user')
  async.each(
    Object.values(mongoose.models),
    (model, cb) =>
      model.ensureIndexes(cb),
    callback
  ) 
}

function createUsers(callback) {
  const users = [
    {username: 'Vasya', password: 'supervasya'},
    {username: 'Vasya', password: 'supervasya2'},
    {username: 'Petya', password: '123'},
    {username: 'admin', password: 'thetruehero'}
  ]
  async.each(
    users,
    (params, cb) => {
      const user = new mongoose.models.User(params)
      user.save(cb)
    },
    callback
  )
}
