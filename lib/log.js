const winston = require('winston'),
  {NODE_ENV = 'development'} = process.env,
  isDev = NODE_ENV == 'development'

function getLogger(module) {
  const localPath = module.filename.replace(process.cwd(), '').substring(1)
  return new winston.Logger({
    transports: [
      new winston.transports.Console({
        colorize: true,
        level: isDev ? 'debug' : 'error',
        label: localPath
      })
    ]
  })
}

module.exports = getLogger
