const WebSocketServer = require('ws').Server,
  http = require('http'),
  express = require('express')

const app = express()
app.use(express.static(__dirname + '/public'))

const server = http.createServer(app)
server.listen(8080)

const webSocketServer = new WebSocketServer({server})
webSocketServer.on('connection', ws => {
  const timer = setInterval(
    () =>
      ws.send(
        JSON.stringify(process.memoryUsage()),
        err => console.error(err)
      ),
    100
  )
  console.log('someone connected')
  ws.on('close', () => {
    console.log('someone disconnected')
    clearInterval(timer)
  })
})